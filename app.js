const tokenBots = require('./tokenBots.json')
require('./replyExtended')

const {Client} = require('discord.js');

class DiscordBot extends Client {
  constructor(props) {
    super(props);

    if (props.token) {
      this.login(props.token).then(() => {
        this.on('ready', () => {
          console.log(`-----------------------------------------`);
          console.log(`Logged in as ${this.user.tag}!`);
          console.log(`Logged in as ${this.user.id}!`);
          console.log(`-----------------------------------------`);
        })

        this.on('message', (message) => {
          if (message.author.bot) return false;

          if (message.content.includes(`<@${this.user.id}>`) || message.content.includes(`<@!${this.user.id}>`)) {
            message.replyExtended('Waf Waf!');
          }
        })
      })
    }
  }
}

for (const tokenBot of tokenBots) {
  new DiscordBot({
    token: tokenBot
  })
}
